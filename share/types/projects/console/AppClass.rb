#!/usr/bin/env ruby
require 'optparse'
# Include your Libraries

# Version of your Command Line Program:
VERSION = [0,0,1]

# Holds your options
options = {}

# Setup your options
OptionParser.new do |opts|
  opts.banner = "Usage: #{File.basename(__FILE__)} [options]"

  # Setup your options, See: http://ruby-doc.org/stdlib-2.0/libdoc/optparse/rdoc/OptionParser.html

  # Default Options
  opts.on_tail("--version","Show Version") do
    puts VERSION.join(".")
    exit
  end
  
  opts.on_tail("-h","--help","Shows this message") do
    puts opts
    exit
  end
end.parse!

# Execute your main code