
ProjectType = Struct.new(
  :parent,
  :name,
  :type,
  :icon,
  :sub_desc,
  :description,
  :creation_cmd
)
