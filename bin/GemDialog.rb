
class GemDialog #(change name)

  include GladeGUI
  
  def initialize(gemdata)
    @specific = gemdata.class == Array ? true : false
    @gemdata = gemdata
    @indx = 0
  end

  def show(parent)
    @old_ui = Gem::DefaultUserInteraction.ui
    @ui = Gem::DefaultUserInteraction.ui = Gem::GtkUI.new
    @dr = Gem::DefaultUserInteraction.ui.download_reporter
    @edit = VR_PseudoTerminal.new(nil)
    @ui.set_console(@edit)
    load_glade(__FILE__, parent)
    set_glade_all() #populates glade controls with insance variables
    @builder["vtelistholder"].add(@dr)
    if specific?
      gemtuple, _ = @gemdata[-1]
      @builder["message"].text = "Installing #{gemtuple.name}-#{gemtuple.version.version}-#{gemtuple.platform} onto your system\nProgress will be displayed below"
    else
      @builder["message"].text = "Updating your local copy of Gems, progress will be displayed below."
    end
    @builder["image1"].pixbuf = Gdk::Pixbuf.new(File.join($THE_PATH,"share","icons","rubygems.png"),32,32)
    @builder["window1"].set_size_request(600,400)
    show_window()
    Thread.new do
      download_gem
    end
  end
  
  def specific?; @specific; end
  
  def download_gem
    total = @gemdata.length * 2
    cur = 0
    if specific?
      @gemdata.each do |x|
        name_tuple, source = x
        spec = source.fetch_spec(name_tuple)
        source.download(spec,Gem.dir)
        cur += 1
        VR::Thread.protect do
          @builder["progress"].fraction = ((cur * 100) / total) * 0.01
          @builder["progress"].text = "#{(cur * 100) / total}% downloaded"
        end
      end
      VR::Thread.protect do
        @builder["vtelistholder"].remove(@dr)
        @builder["vtelistholder"].add(@edit)
        @builder["window1"].show_all
      end
      sleep(1)
      install_gem
    else
    end
  end
  
  def install_gem
    total = @gemdata.length * 2
    cur = @gemdata.length
    aborted = false
    if specific?
      @gemdata.each do |x|
        @ui.say("Installing #{x[0].name + "-" + x[0].version.version}...")
        options = { :ignore_dependencies => true }
        options[:user_install] = true unless File.writable? Gem.dir
        installer = Gem::Installer.new(File.join(Gem.dir,"cache",x[0].name + "-" + x[0].version.version + ".gem"),options)
        begin
          installer.install
        rescue => err
          @ui.say("Failed to install #{x[0].name}-#{x[0].version.version}.gem")
          @ui.say("Error Log:")
          @ui.say("Error #{err.class}")
          @ui.say("#{err.message}")
          aborted = true
          break
        end
        @ui.say("Installed gem #{x[0].name + "-" + x[0].version.version}.")
        cur += 1
        VR::Thread.protect do
          @builder["progress"].fraction = ((cur * 100) / total) * 0.01
          @builder["progress"].text = "#{(cur * 100) / total}% Installed"
        end
      end
      if aborted
        @ui.say("\n\nFailed to install requested gem(s).")
        @builder["canclose"].sensitive = true
        @builder["progress"].text = "Failed"
        @builder["progress"].fraction = 1.0
      else
        @ui.say("\n\nInstallation Complete.")
        VR::Thread.protect do
          @builder["canclose"].sensitive = true
          @builder["progress"].text = "Completed"
        end
      end
    else
    end
  end

  def dr__download_done(reporter,filename)
  end
  
  def canclose__clicked(*args)
    destroy_window()
  end

end
