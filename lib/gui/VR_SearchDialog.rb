module VR
  module IDE
    module SearchDialog
      def init_search()
        if VR::IDE::Settings[:search_history].empty?
          @builder["search_box"].add(@search = VR::SimpleComboBoxEntry.new("",""))
        else
          @builder["search_box"].add(@search = VR::SimpleComboBoxEntry.new(
              VR::IDE::Settings[:search_history].last,*VR::IDE::Settings[:search_history]))
        end
      end
      
      def init_replace()
        if VR::IDE::Settings[:replace_history].empty?
          @builder["replace_box"].add(@replace = VR::SimpleComboBoxEntry.new("",""))
        else
          @builder["replace_box"].add(@replace = VR::SimpleComboBoxEntry.new(
              VR::IDE::Settings[:replace_history].last,*VR::IDE::Settings[:replace_history]))
        end
      end
      
      def toggle_find()
        if @search.active_text == ""
          @builder["find"].sensitive = false
        else
          @builder["find"].sensitive = true
        end
      end
      
      def toggle_replace()
        if @replace.active_text == ""
          @builder["repall"].sensitive = false
        else
          @builder["repall"].sensitive = true
        end
      end
      
      def fill_options()
        options = {
          :matchcase => nil,
          :matchword => nil,
          :backwards => nil,
          :regex => nil,
          :wraparound => nil
        }
        options.each_key do |k|
          options[k] = @builder[k.to_s].active?
        end
        options
      end

      def update_search_history(string,history=:search_history)
        unless VR::IDE::Settings[history].index(string)
          VR::IDE::Settings[history] << string
          if VR::IDE::Settings[history].length > 15
            VR::IDE::Settings[history].delete_at(0)
          end
          VR::IDE::Settings.save
        else
          VR::IDE::Settings[history].delete(string)
          VR::IDE::Settings[history] << string
          VR::IDE::Settings.save
        end
      end
      
      def find__clicked(*args)
        string = @search.active_text
        update_search_history(string)
        
        search_options = fill_options()
        ret = @main.doc_manager.get_current_doc.find_text(string,search_options)
        if ret == false
          @builder["rep"].sensitive = false unless @builder["rep"].nil?
          VR::Dialog.message_box("Unable to find '#{string}' in current document","VisualRuby Find")
        else
          @builder["rep"].sensitive = true unless @builder["rep"].nil?
        end
      end
      
      def close__clicked(*args)
        destroy_window()
      end
    end
  end
end