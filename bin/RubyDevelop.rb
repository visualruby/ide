
class RubyDevelop #(change name)

  attr_reader :builder, :doc_manager, :project, :projview
  include GladeGUI

  def show()
    load_glade(__FILE__)  #loads file, glade/MyClass.glade into @builder
    
    VR::IDE::Settings.load
    
    icon = File.join($THE_PATH,"share","icons","vr.png")
    @builder["window1"].icon_list = [Gdk::Pixbuf.new(icon,128,128),Gdk::Pixbuf.new(icon,64,64),Gdk::Pixbuf.new(icon,32,32),
      Gdk::Pixbuf.new(icon,16,16)]
    set_glade_all(self) #populates glade controls with insance variables (i.e. Myclass.var1)
    @doc_manager = VR_DocumentManager.new(self,@builder["VR_Documents"])
    @doc_manager.add()
    
    @projview = Gtk::Label.new("No current project open")
    @builder["VR_ProjectScroll"].add_with_viewport(@projview)

    @tab_terminals = Gtk::Notebook.new
    @tab_terminals.tab_pos = Gtk::POS_TOP
    @output = VR_PseudoTerminal.new(self)
    sw = Gtk::ScrolledWindow.new
    sw.add(@output)
    sw.hscrollbar_policy = Gtk::POLICY_AUTOMATIC
    sw.vscrollbar_policy = Gtk::POLICY_AUTOMATIC
    @appicon = Gdk::Pixbuf.new(File.join($THE_PATH,"share","icons","application.png"),16,16)
    @termicon = Gdk::Pixbuf.new(File.join($THE_PATH,"share","icons","terminal.png"),16,16)
    @tab_terminals.append_page(sw,VR_TabLabel.new(@appicon,"Application Output",false))
    if LoadedFeatures.has_feature? "vte"
      @term = Vte::Terminal.new
      sw = Gtk::ScrolledWindow.new
      sw.add(@term)
      sw.hscrollbar_policy = Gtk::POLICY_NEVER
      sw.vscrollbar_policy = Gtk::POLICY_AUTOMATIC
      @term.set_font("Monospace 10", Vte::TerminalAntiAlias::FORCE_ENABLE)
      @term.scrollback_lines = 5000
      @term.signal_connect("child-exited") do |widget|
        widget.reset(true,true)
        thePath = VR::IDE::get_project_folder
        if @projview.class != Gtk::Label
          thePath = @projview.path
        end
        widget.fork_command(:argv=>["bash","--login"],:working_directory => thePath)
      end
      @term.fork_command(:argv=>["bash","--login"],:working_directory => VR::IDE.get_project_folder)
      @tab_terminals.prepend_page(sw,VR_TabLabel.new(@termicon,"Terminal",false))
    end
    @builder["VR_OutputHolder"].add(@tab_terminals)
    
    @builder["VR_ToolExecute"].sensitive = false
    @builder["VR_MenuRepository"].submenu = @builder["VR_MenuRepoInit"]
    
    ag = Gtk::AccelGroup.new
    ag.connect(Gdk::Keyval::GDK_N,Gdk::Window::CONTROL_MASK,Gtk::ACCEL_VISIBLE) { VR_MenuNewFile__activate() }
    
    @builder["imageview"].signal_connect("delete-event") do
      @builder["imageview"].hide
      true
    end
    
    @builder["vpaned1"].position = 260
    
    @builder["window1"].add_accel_group(ag)
    
    show_window()
  end
  
  #region Helper Functions
  def load_project(projfile)
    if not @project.nil?
      @project.last_session[:tree] = @projview.get_open_folders()
      @project.last_session[:open] = @doc_manager.open_documents
      @project.last_session[:current_file] = @doc_manager.get_current()
      @project.save
    end
    if @projview.class != VR_FileTreeView
      @builder["VR_ProjectScroll"].remove(@builder["VR_ProjectScroll"].children[0])
      @projview.destroy()
      @projview = VR_FileTreeView.new(self,File.join($THE_PATH,"share","icons","files"),22,22)
      @builder["VR_ProjectScroll"].add(@projview)
      @projview.show
    end
    @doc_manager.close_all()
    @git = nil
    @hg = nil
    
    @project = ProjectFile.load(projfile)
    @projview.path = @project.path
    @projview.open_folders(@project.last_session[:tree])
    @project.last_session[:open].each do |file|
      @doc_manager.add(file)
    end
    @builder["window1"].title = "RubyDevelop - [#{@project.name}]"
    @doc_manager.switch_to(@project.last_session[:current_file]) unless @project.last_session[:current_file].nil?
    @builder["VR_ToolExecute"].sensitive = true
    @term.feed_child("exit\n")
    
    begin
      @git = Git.open(@project.path)
    rescue ArgumentError
      @git = nil
    end
    
    begin
      @hg = Mercurial::Repository.open(@project.path)
      @hg.commits.count
    rescue Mercurial::CommandError
      @hg = nil
    end
    
    if @git.nil? && @hg.nil?
      @builder["VR_MenuRepository"].remove_submenu
      @builder["VR_MenuRepository"].submenu = @builder["VR_MenuRepoInit"]
    else
      @builder["VR_MenuRepository"].remove_submenu
      @builder["VR_MenuRepository"].submenu = @builder["VR_MenuRepoEdit"]
    end
  end

  def execute_subproc(cmd,file="")
    if cmd.class == Array
      cmdtxt = cmd.join(" ")
    elsif cmd.class == String
      cmdtxt = cmd
      cmd = cmd.split(" ")
    end
    proc = VR::Process.new(cmd,@project.path)

    proc.signal_connect("stdout") do |lproc,data|
      @output.append_text(data)
    end

    proc.signal_connect("stderr") do |lproc,data|
      @output.append_error(data)
    end

    proc.signal_connect("exited") do |lproc|
      @builder["VR_ToolExecute"].sensitive = true
      @builder["pvm_execute"].sensitive = true
      @output.append_text("\nExecution completed.\n")
      @output.append_text("Exited with status: #{proc.exitstatus}")
      @output.hilight_links(true)
    end

    @output.clear()
    if file.empty?
      @output.append_text("Executing Ruby Script\n\n#{@project.path}> #{cmdtxt}\n")
    else
      @output.append_text("Executing Ruby Script\n\n#{File.dirname(file)}> #{cmdtxt}\n")
    end
    begin
      if file.empty?
        proc.pexecute(@project.path)
      else
        proc.pexecute(File.dirname(file))
      end
      @builder["VR_ToolExecute"].sensitive = false
      @builder["pvm_execute"].sensitive = false
    rescue => e
      @output.append_error("Error: #{e.to_s}")
    end
    @output.append_text("Process started with pid #{proc.pid}\n")
  end
  #endregion

  #region Start of code for Event Handlers

  #region Menu Handlers
  #  Menu > File > ...
  def VR_MenuNewProject__activate(*args)
    dlg = NewProjectDialog.new(self)
    dlg.show(self)
  end

  def VR_MenuNewFile__activate(*args)
    @doc_manager.add()
  end

  def VR_MenuOpen__activate(*args)
    dialog = Gtk::FileChooserDialog.new("Open a File or Project...",
                  @builder["window1"],
                  Gtk::FileChooser::ACTION_OPEN,
                  nil,
                  [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                  [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    filter = Gtk::FileFilter.new
    filter.add_pattern("*.rbproj")
    filter.add_pattern("*.rb")
    filter.add_pattern("*.yaml")
    filter.add_pattern("*.rhtml")
    filter.add_pattern("*.html")
    filter.add_pattern("*")
    dialog.set_filter(filter)
    dialog.current_folder = VR::IDE.get_project_folder
    if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
      if dialog.filename =~ /.rbproj$/
        # Load Project
        load_project(dialog.filename)
      else
        @doc_manager.add(dialog.filename)
      end
    end
    dialog.hide
  end

  def VR_MenuSave__activate(*args)
    @doc_manager.save_current()
  end

  def VR_MenuSaveAs__activate(*args)
    @doc_manager.save_current(true)
  end
  
  def VR_MenuClose__activate(*args)
  end

  def VR_MenuCloseProject__activate(*args)
    @project.last_session[:tree] = @projview.get_open_folders()
    @project.last_session[:open] = @doc_manager.open_documents
    @project.last_session[:current_file] = @doc_manager.get_current()
    @project.save
    @doc_manager.try_to_save_all()
    @doc_manager.close_all()
    @builder["VR_ProjectScroll"].remove(@projview)
    @projview = Gtk::Label.new("No current project open")
    @builder["VR_ProjectScroll"].add_with_viewport(@projview)
    @project = nil
    @projview.show
    @builder["VR_ToolExecute"].sensitive = false
  end

  def VR_MenuQuit__activate(*args)
    unless @project.nil?
      @project.last_session[:tree] = @projview.get_open_folders()
      @project.last_session[:open] = @doc_manager.open_documents
      @project.last_session[:current_file] = @doc_manager.get_current()
      @project.save
    end
    return unless @doc_manager.try_to_save_all()
    destroy_window()
  end
  
  # Menu > Edit > ...
  
  def VR_MenuPreferences__activate(*args)
    dlg = SettingsDialog.new
    dlg.show(self)
  end
  
  # Menu > Search > ...
  def VR_MenuFind__activate(*args)
    dlg = FindDialog.new
    dlg.show(self)
  end
  
  def VR_MenuReplace__activate(*args)
    dlg = FindReplaceDialog.new
    dlg.show(self)
  end
  
  # Menu > Repository > ...
  
  # Menu > Tools > ...
  def VR_MenuGemManager__activate(*args)
    dlg = GemManager.new
    dlg.show(self)
  end
  
  # Menu > Help > ...
  #endregion

  #region Toolbar Handlers
  def VR_ToolNew__clicked(*args)
    @doc_manager.add()
  end

  def VR_ToolOpen__clicked(*args)
    VR_MenuOpen__activate(*args)
  end

  def VR_ToolSave__clicked(*args)
    @doc_manager.save_current()
  end

  def VR_ToolSaveAs__clicked(*args)
    @doc_manager.save_current(true)
  end
  
  def VR_ToolExecute__clicked(*args)
    return if @project.nil?
    return if @project.options[:exec].nil?
    @doc_manager.try_to_save_all()
    cmd = @project.options[:exec]
    execute_subproc(cmd)
  end
  
  def VR_ToolAbout__clicked(*args)
    p 1/0
  end
  #endregion
  
  #region projviewmenu Handlers
  def pvm_new__activate(*args)
    path = @projview.get_selected_path()
    path = File.dirname(path) if File.file?(path)
    folder = ""
    dlg = @builder["foldercreate"]
    if dlg.run == 1
      folder = @builder["folder"].text
      @builder["folder"].text = ""
    end
    dlg.hide
    return if folder.empty?
    FileUtils.mkdir(File.join(path,folder))
    @projview.refresh(@projview.path)
  end
  
  def pvm_rename__activate(*args)
    path = @projview.get_selected_path()
    dlg = @builder["renameffdlg"]
    @builder["oldname"].text = path
    if File.file? path
      @builder["foldfile"].stock == Gtk::Stock::FILE
    else
      @builder["foldfile"].stock == Gtk::Stock::DIRECTORY
    end
    if dlg.run == 1
      newpath = @builder["newname"].text
      if newpath != ""
        newpath = File.join(File.dirname(path),newpath)
        FileUtils.mv(path,newpath)
      end
    end
    dlg.hide
    @projview.refresh(@projview.path)
  end
  
  def pvm_remove__activate(*args)
    path = @projview.get_selected_path()
    dlg = @builder["promptdelete"]
    if File.file?(path)
      dlg.text = "Are you sure you want to delete this file?"
    elsif File.directory?(path)
      test = Dir.glob(File.join(path,"**","*"))
      if test.length != 0
        dlg.text = "Are you sure you want to delete this folder, and all the files and folders underneath it?"
      else
        dlg.text = "Are you sure you want to delete this folder?"
      end
    end
    if dlg.run == Gtk::Dialog::RESPONSE_YES
      FileUtils.rm_rf(path)
    end
    dlg.hide
    @projview.refresh(@projview.path)
  end
  
  # pvm_scm > ...
  
  
  # pvm_...
  
  def pvm_execute__activate(*args)
    path = @projview.get_selected_path()
    if File.directory?(path)
      cmd = @project.options[:exec]
      file = ""
    else
      # Check for Rakefile regex match File.basename(path) =~ /[R|r]akefile.?/
      if File.extname(path) == ".rb"
        cmd = ["ruby",path]
        file = path
      elsif File.extname(path) == ""
        if GLib.os_win32?
          cmd = ["cmd","/c",path]
        else
          cmd = ["bash",path]
        end
        file = path
      end
    end
    execute_subproc(cmd,file)
  end
  
  #endregion

  #region Window1 Handlers
  def on_window1_delete_event
    unless @project.nil?
      @project.last_session[:tree] = @projview.get_open_folders()
      @project.last_session[:open] = @doc_manager.open_documents
      @project.last_session[:current_file] = @doc_manager.get_current()
      @project.save
    end
    return true unless @doc_manager.try_to_save_all()
    return false
  end
  #endregion

  #endregion End of code for Event Handlers

end
