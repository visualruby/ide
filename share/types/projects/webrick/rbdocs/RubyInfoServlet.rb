class RubyInfoServlet < WEBrick::HTTPServlet::AbstractServlet
  # Helper Methods
  def generate_table(hash)
    str = ""
    hash.each do |key,val|
      temp = ERB.new(File.read(File.join($THE_PATH,"rbdocs","ri_table_entry.erb")))
      the_key = ""
      the_val = ""
      if key.class == String
        the_key = WEBrick::HTMLUtils.escape(key)
      elsif key.class == Symbol
        the_key = WEBrick::HTMLUtils.escape(humanize(key))
      else
        the_key = WEBrick::HTMLUtils.escape(key.inspect)
      end
      if val.class != String
        the_val = WEBrick::HTMLUtils.escape(val.inspect)
      else
        the_val = WEBrick::HTMLUtils.escape(val)
      end
      str += temp.result(binding)
    end
    str
  end

  def humanize(sym)
    parts = sym.to_s.split(/(?=[A-Z])/)
    lparts = []
    concat = ""
    parts.each do |part|
      if part.length == 1
        concat += part
      else
        if concat.length > 1
          lparts << concat
          concat = ""
        end
        lparts << part
      end
    end
    lparts.join(" ")
  end

  # Template methods (For displaying information)
  def ruby_environment
    hash = {:RubyVersion => RUBY_VERSION,
        :RubyPatchLevel => RUBY_PATCHLEVEL,
        :RubyPlatform => RUBY_PLATFORM,
        :RubyReleaseDate => RUBY_RELEASE_DATE,
        :RubyCopyright => RUBY_COPYRIGHT,
        "WEBrick Version" => WEBrick::VERSION}
    return generate_table(hash)
  end

  def webrick_environment
    config = server_request.instance_variable_get(:@config).dup
    config.delete(:MimeTypes)
    generate_table(config)
  end

  def mime_types
    config = server_request.instance_variable_get(:@config).dup
    generate_table(config[:MimeTypes])
  end

  def headers
    generate_table(server_request.header)
  end

  def server_environment
    generate_table(ENV)
  end

  attr_reader :server_request
  def do_GET(req,resp)
    @server_request = req
    resp["Content-Type"] = "text/html"
    resp.status = 200
    temp = ERB.new(File.read(File.join($THE_PATH,"rbdocs","ri_template.erb")))
    resp.body = temp.result(binding)
  end
end