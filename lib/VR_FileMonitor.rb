class VR_FileMonitor < GLib::Object
  type_register

  signal_new("new_entry",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      String)

  signal_new("del_entry",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      String)
  
  attr_accessor :path, :recursive
  
  def initialize(file_path,recursive=true)
    super()
    @path = file_path
    @recursive = recursive
  end
  
  def start()
    acquire_ruby_monitor()
  end
  
  def stop()
    release_ruby_monitor()
  end
  
  private

  def signal_do_new_entry(path)
  end

  def signal_do_del_entry(path)
  end

  def acquire_ruby_monitor()
    if @recursive
      @globber = File.join(@path,"**","*")
    else
      @globber = @path
    end
    
    @thread = Thread.new do
      state = Dir.glob(@globber)
      loop do
        sleep(0.5)
        check = Dir.glob(@globber)
        if not (check - state).empty? # New File / Directory
          diff = check - state
          diff.each do |path|
            self.signal_emit("new_entry",path)
          end
          state = check
        elsif not (state - check).empty? # Removed File / Directory
          diff = state - check
          diff.each do |path|
            self.signal_emit("del_entry",path)
          end
          state = check
        end
      end
    end
  end
  
  def release_ruby_monitor()
    @thread.kill
  end
end
