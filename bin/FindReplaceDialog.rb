
class FindReplaceDialog

  include GladeGUI
  include VR::IDE::SearchDialog

  def show(parent)
    load_glade(__FILE__,parent)
    @main = parent
    
    set_glade_all(self)
    
    # Search Box
    init_search()
    @search.signal_connect("changed") { toggle_find() }
    toggle_find()
    
    # Replace Box
    init_replace()
    @replace.signal_connect("changed") { toggle_replace() }
    @builder["rep"].sensitive = false
    toggle_replace()
    
    @main.doc_manager.get_current_doc.reset_find()
    
    show_window()
  end
  
  def rep__clicked(*args)
    string = @replace.active_text
    unless VR::IDE::Settings[:replace_history].index(string)
      VR::IDE::Settings[:replace_history] << string
      if VR::IDE::Settings[:replace_history].length > 15
        VR::IDE::Settings.delete_at(0)
      end
      VR::IDE::Settings.save
    else
      VR::IDE::Settings[:replace_history].delete(string)
      VR::IDE::Settings[:replace_history] << string
      VR::IDE::Settings.save
    end
    ret = @main.doc_manager.get_current_doc.replace_text(string)
    if ret == false
      VR::Dialog.message_box("Failed to replace '#{@search.active_text}' with '#{string}' in current document","VisualRuby Replace")
    else
      find__clicked(*args)
    end
  end
  
  def repall__clicked(*args)
    the_doc = @main.doc_manager.get_current_doc()
    the_doc.reset_find()
    the_search = @search.active_text
    the_replace = @replace.active_text
    the_options = fill_options()
    count = 0
    while (the_doc.find_text(the_search,the_options))
      the_doc.replace_text(the_replace)
      count += 1
    end
    VR::Dialog.message_box("Replaced #{count} #{count > 1 ? "occurances" : "occurance"} of '#{the_search}' with '#{the_replace}'","VisualRuby Replace")
  end
  
  def on_window1_delete_event()
    destroy_window()
  end
end