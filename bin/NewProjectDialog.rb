
class NewProjectDialog #(change name)

  include GladeGUI
  def initialize(main)
    @main = main
    @projtypes = {}
    @projlocs = {}
  end

  def ColorParse(red,green,blue)
    a = blue + ( green * 256) + ( red * 256 * 256)
    Gdk::Color.parse(sprintf("#%06x",a))
  end

  def validate_input
    if !(@builder["project_list"].selected_items[0].nil?) and
        @builder["project_name"].text != "" and File.exists? @builder["project_location"].text
      @builder["forward"].sensitive = true
    else
      @builder["forward"].sensitive = false
    end
    @builder["project_creation_loc"].text = "Project will be created at: #{File.join(@builder["project_location"].text,@builder["project_name"].text)}"
  end

  def show(parent)
    load_glade(__FILE__,parent)

    @builder["notebook1"].show_tabs = false

    @builder["project_type_desc"].modify_base(Gtk::STATE_NORMAL,ColorParse(210,210,210))
    @builder["project_type_desc"].set_cursor_visible(false)
    @builder["project_type_desc"].buffer.create_tag("lbTag",{"size" => 12 * Pango::SCALE, "weight" => Pango::FontDescription::WEIGHT_BOLD})

    # Load up the Project information
    Dir.glob(File.join($THE_PATH,"share","types","**","project.yaml")).each do |proj|
      pt_def = YAML::load(IO.read(proj))
      unless @projtypes.has_key? pt_def[:parent]
        @projtypes[pt_def[:parent]] = {pt_def[:name] => pt_def}
      else
        @projtypes[pt_def[:parent]][pt_def[:name]] = pt_def
      end
      @projlocs[pt_def[:name]] = File.dirname(proj)
    end

    # Setup our Project Tree
    @builder["project_tree"].model = tmodel = Gtk::TreeStore.new(String)

    iter = tmodel.append(nil)
    iter[0] = "Recent"
    
    @projtypes.each_key do |key|
      iter = tmodel.append(nil)
      iter[0] = key
    end
    renderer = Gtk::CellRendererText.new
    column = Gtk::TreeViewColumn.new("Project Type",renderer, :text => 0)
    @builder["project_tree"].append_column(column)

    # Setup our Project List
    @builder["project_list"].model = Gtk::ListStore.new(String, Gdk::Pixbuf, String)
    @builder["project_list"].markup_column = 0
    @builder["project_list"].pixbuf_column = 1

    # Setup Default Fields
    @builder["project_location"].text = VR::IDE.get_project_folder

    # Finish Population
    set_glade_all() #populates glade controls with insance variables
    show_window()
  end

  def project_tree__cursor_changed(*args)
    iter = @builder["project_tree"].selection.selected
    return if iter.nil?
    text = iter[0]
    @builder["project_list"].model.clear
    if @projtypes.has_key? text
      @projtypes[text].each do |key,val|
        iter = @builder["project_list"].model.append
        iter[0] = "<span font_size='medium' font_weight='bold'>#{key}</span>\n<span font_size='small' fgcolor='#919191'>#{val[:sub_desc]}</span>"
        iter[1] = Gdk::Pixbuf.new(File.join($THE_PATH,"share","icons",val[:icon]),32,32)
        iter[2] = key
      end
    end
    @builder["project_list"].item_width = @builder["project_list"].allocation.width - 25
    validate_input
  end

  def project_list__selection_changed(*args)
    iter = @builder["project_tree"].selection.selected
    return if iter.nil?
    category = iter[0]
    item = @builder["project_list"].selected_items[0]
    return if item.nil?
    iter = @builder["project_list"].model.get_iter(item)
    return if iter.nil?
    project = iter[2]
    data = @projtypes[category][project][:description]
    icon = File.join($THE_PATH,"share","icons",@projtypes[category][project][:icon])
    theBuffer = @builder["project_type_desc"].buffer
    theBuffer.text = ""
    theBuffer.insert(theBuffer.end_iter,Gdk::Pixbuf.new(icon,32,32))
    theBuffer.insert(theBuffer.end_iter,project + "\n","lbTag")
    theBuffer.insert(theBuffer.end_iter,data)
    validate_input
  end

  def project_name__changed(*args)
    validate_input
  end

  def project_location__changed(*args)
    validate_input
  end

  def browse__clicked(*args)
    # Show directory dialog
    dlg = Gtk::FileChooserDialog.new("Project Location",
            @builder["window1"], Gtk::FileChooser::ACTION_SELECT_FOLDER,
            nil,
            [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
            [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    dlg.current_folder = @builder["project_location"].text
    if dlg.run == Gtk::Dialog::RESPONSE_ACCEPT
      @builder["project_location"].text = dlg.filename
    end
    dlg.destroy
  end

  def cancel__clicked(*args)
    destroy_window
  end

  def forward__clicked(*args)
    if @builder["forward"].label == "Finish"
      create_project()
      destroy_window()
      return
    end
    @builder["notebook1"].next_page
    if (@builder["notebook1"].n_pages - 1) == @builder["notebook1"].page
      @builder["forward"].label = "Finish"
    end
  end

  def create_project()
    project_name = @builder["project_name"].text
    project_path = @builder["project_location"].text
    app_class_name = @builder["project_name"].text
    if app_class_name.match(/[-|_]/)
      app_class_name = app_class_name.split(/[-|_]/).collect {|x| x.capitalize!}.join("")
    else
      app_class_name.capitalize!
    end
    
    the_path = File.join(@builder["project_location"].text,project_name)
    iter = @builder["project_list"].model.get_iter(@builder["project_list"].selected_items[0])
    skel = @projlocs[iter[2]]
    
    rt = RubyTemplate.new({:app_class_name => app_class_name,:project_name=>project_name,:project_path=>project_path})
    
    FileUtils.mkdir_p(the_path)
    Dir.glob(File.join(skel,"**","*")) do |file|
      next if File.basename(file) == "project.yaml"
      if File.directory? file
        thefile = file.dup
        thefile[skel + "/"] = ""
        FileUtils.mkdir_p(File.join(the_path,thefile))
      else
        template = IO.read(file)
        thefile = file.dup
        thefile[skel + "/"] = ""
        thefile.gsub!(/AppClass/,app_class_name) if thefile.match(/AppClass/)
        thefile.gsub!(/ProjName/,project_name) if thefile.match(/ProjName/)
        File.open(File.join(the_path,thefile),"w") do |fh|
          fh.print(rt.render(template))
        end
      end
    end
    
    @main.load_project(File.join(the_path,project_name + ".rbproj"))
  end

end
