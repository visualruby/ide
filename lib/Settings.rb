module VR
  module IDE
    class Settings
      include Singleton
      class << self
        def load
          if File.exists?(File.join(ENV["HOME"],".visualruby")) &&
              File.exists?(File.join(ENV["HOME"],".visualruby","settings.yaml"))
            data = YAML::load(IO.read(File.join(ENV["HOME"],".visualruby","settings.yaml")))
            instance.instance_variable_get(:@options).merge!(data) do |key,old,new|
              if old.class == Hash
                old.merge(new)
              else
                old + new
              end
            end
          end
        end
        
        def save
          Dir.mkdir(File.join(ENV["HOME"],".visualruby")) unless File.exists?(File.join(ENV["HOME"],".visualruby"))
          File.open(File.join(ENV["HOME"],".visualruby","settings.yaml"),"w") do |fh|
            fh.print YAML::dump(instance.instance_variable_get(:@options))
          end
        end
        
        def [](key)
          instance.instance_variable_get(:@options)[key]
        end
        
        def []=(key,val)
          instance.instance_variable_get(:@options)[key] = val
        end
      end
      
      def initialize()
        @options = {
          :general => {
            :glade => "glade3",
            :browser => "firefox"
          },
          :window => {
            :pos_x => -1,
            :pos_y => -1,
            :width => -1,
            :height => -1,
            :hsplit => -1,
            :vsplit => -1,
            :show_sidebar => true,
            :show_output => true
          },
          :editor => {
            :highlight_current_line => true,
            :show_right_margin => true,
            :auto_indent => true,
            :insert_spaces_instead_of_tabs => true,
            :show_line_numbers => true,
            :right_margin_position => 80,
            :tab_width => 2,
            :indent_width => 2,
            :style_scheme => "classic"
          },
          :search_history => [],
          :replace_history => [],
          :rubygems => {
            :logins => { }
          }
        }
      end
    end
  end
end