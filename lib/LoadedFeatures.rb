module LoadedFeatures
  def feature_loaded?(name)
    $LOADED_FEATURES.collect {|x| x =~ /#{name}.so/}.compact.length == 1
  end
  
  def has_feature?(name)
    feature_loaded? name
  end
  
  module_function :feature_loaded?
  module_function :has_feature?
end