require 'glib2'
require 'open3'
require '../lib/Process'

STDOUT.sync = true
STDERR.sync = true

trace = TracePoint.new(:call,:thread_begin,:thread_end) do |tp|
  puts "%0.3d> %s->%s %s" % [tp.lineno, tp.defined_class, tp.method_id, tp.event]
end

exit_script = false

proc = VR::Process.new(["ruby","test_shell.rb"])

proc.signal_connect("stdout") do |lproc,data|
  puts "stdout(#{proc.pid}): #{data}"
end

proc.signal_connect("stderr") do |lproc,data|
  puts "stderr(#{proc.pid}): #{data}"
end

proc.signal_connect("exited") do
  puts "Proc #{proc.pid} exited with status: #{proc.exitstatus}"
  exit_script = true
end

proc.pexecute(Dir.pwd)

loop do
  sleep(0.5)
  break if exit_script
end