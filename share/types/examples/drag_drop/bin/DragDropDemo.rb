
class DragDropDemo

  include GladeGUI

  def show()
    load_glade(__FILE__)  #loads file, glade/MyClass.glade into @builder
    @treeview1 = "I'm a widget named textview2.\n\nTry dragging a file or the button onto me."
    @view = VR::FileTreeView.new("./img")
    @view.refresh()
    @builder["scrolledwindow1"].add(@view)
    @view.drag_to(@builder["textview1"])
    @view.drag_to(@builder["button1"])
    @builder["button1"].drag_to(@builder["textview1"])
    @builder["textview1"].drag_to(@builder["button1"])
    set_glade_all
    show_window() 
  end  


#    This executes when button1 receives a widget dropped on it.  When the button1 widget
#    receives a drag_drop signal, an instance variable named dragged_widget is set automatically set
#    by visualruby containing a reference to the dragged widget:

  def button1__drag_drop(*args)
    but1 = @builder["button1"]
    if but1.dragged_widget.is_a?(Gtk::TextView)
      but1.label = but1.dragged_widget.buffer.text.slice(0,15) 
    elsif but1.dragged_widget.is_a?(VR::FileTreeView)
      but1.label = @view.get_selected_path()
    end
  end

  def textview1__drag_drop(*args)
    tv1 = @builder["textview1"]
    if tv1.dragged_widget == @view
      path = @view.get_selected_path()
      tv1.buffer.text = File.directory?(path) ? path :  File.read(path)
    elsif tv1.dragged_widget.is_a?(Gtk::Button)
      tv1.buffer.text = tv1.dragged_widget.label
    end
  end  

end




