# VisualRuby2

VisualRuby2 is my current implementation of the next-gen IDE for 
VisualRuby.  Currently it's codenamed VisualRuby2, till blessings can be 
given from the original Author of VisualRuby Eric Cunningham.

VisualRuby2 will implement various features that are common to other 
IDE's, that are currently missing, as well as a more structured, and 
familiar way in which to develop Ruby Applications varying from Ruby/GTK 
to Rails to WEBrick, and any other project type out there that can be 
incuded.

The main focus for VisualRuby2 though, is to simply develop for the 
outline of Ruby/GTK GUI Development, as it's original intention, 
leveraging the vrlib, to make GTK development in Ruby more familiar to 
Visual Basic developers.

Check back often as more features are added to the repository for the 
code.

Mario Steele - Developer
