class RubyGemsAPI
  
  def initialize
    uri = URI.parse("https://rubygems.org")
    @http = Net::HTTP.new(uri.host,uri.port)
    @http.use_ssl = true
    @http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  end
  
  def get_key
    return Gem.configuration.rubygems_api_key
  end
  
  def setup_key(username,password)
    path = "/api/v1/api_key.yaml"
    key = nil
    @http.start do |h|
      req = Net::HTTP::Get.new(path)
      req.basic_auth username, password
      response = h.request(req)
      obj = YAML::load(response.body)
      key = obj[:rubygems_api_key]
      if key
        Gem.configuration.rubygems_api_key = key
      else
        VR::Dialog.message_box(obj.values[0])
      end
    end
    return key
  end
  
  def has_key?
    return File.file?(Gem.configuration.credentials_path) && !(Gem.configuration.rubygems_api_key.nil?)
  end
  
  def get_obj_url(path)
    return nil unless key = get_key()
    @http.start() do |h|
      req = Net::HTTP::Get.new(path)
      response = h.request_get(path, 'Authorization' => key)
      return obj = YAML.load(response.body)
    end
  end
  
  def push_gem(fn)
    return unless key = get_key()
    path = "https://rubygems.org/api/v1/gems"
    req = Net::HTTP::Post.new(path)
    req.add_field('Authorization',key)
    req.content_type = 'application/octet-stream'
    bod = File.open(fn,"rb") {|io| io.read}
    req.body = bod
    req.content_length = req.body.size
    @http.start() do |h|
      response = h.request(req)
      return response.message
    end
  end
  
  def yank_gem(name, ver)
    return unless key = get_key()
    path = "/api/v1/gems/yank?gem_name=#{URI.encode(name)}&version=#{URI.encode(ver)}"
    req = Net::HTTP::Delete.new(path)
    req.add_field('Authorization',key)
    req.add_field('Connection', 'keep-alive')
    req.add_field('Keep-Alive', '30')
    req.content_type = "application/x-www-form-urlencoded"
    @http.start() do |h|
      response = h.request(req)
      return response.message
    end
  end
  
end