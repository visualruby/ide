
class VR_DocumentManager
  def initialize(main,notebook)
    @main = main
    @notebook = notebook
    @pages = {}
    @untitled = -1
  end

  def remove_tab(file)
    controls = @pages.delete(file)
    num = @notebook.page_num(controls[:tab])
    @notebook.remove_page(num)
    @untitled -= 1 if File.basename(file) =~ /Untitled(\d+)?\.rb/
  end
  private :remove_tab

  def remove(file="Untitled")
    remove_tab(file)
  end

  def remove_all()
    thePages = @pages.keys
    thePages.each do |x|
      remove(x)
    end
  end

  alias_method :close, :remove
  alias_method :close_all, :remove_all
  
  def each_doc
    @pages.each do |k,v|
      yield v[:doc]
    end
  end

  def open_documents
    @pages.keys
  end

  def add(file="Untitled")
    if file == "Untitled"
      if @untitled < 0
        file = File.join(Dir.pwd,"Untitled.rb")
        @untitled = 0
      else
        file = File.join(Dir.pwd,"Untitled#{@untitled += 1}.rb")
      end
    end
    return if switch_to(file)
    label = VR_TabLabel.new(nil,File.basename(file))
    doc = VR_Document.new(file,label,@main)
    tab = Gtk::ScrolledWindow.new
    tab.add(doc)
    tab.set_policy(Gtk::POLICY_AUTOMATIC,Gtk::POLICY_ALWAYS)
    @notebook.append_page(tab,label)
    @notebook.show_all
    @pages[file] = {:tab=>tab,:doc=>doc,:label=>label}

    label.signal_connect("close_clicked") do |widget|
      theFile = nil
      @pages.each do |mfile, gtk|
        if gtk[:doc] == doc
          theFile = mfile
          break
        end
      end
      return if theFile.nil?

      if @pages[theFile][:doc].buffer.modified?
        d = @main.builder["promptsave"]
        resp = d.run
        d.hide
        if resp == 2 # Save Changes
          save_document(theFile)
          remove_tab(theFile)
        elsif resp == 1 # Don't save the Changes
          remove_tab(theFile)
        else # Cancelled closing action
          # Don't do anything here, just ignore the closing request.
        end
      else
        remove_tab(theFile)
      end
    end
    switch_to(file)
  end
  
  alias_method :load_tab, :add

  def switch_to(file)
    if @pages.has_key? file
      num = @notebook.page_num(@pages[file][:tab])
      @notebook.set_page(num)
      true
    else
      false
    end
  end

  def save_document(file,save_as = false)
    if @pages.has_key? file
      # We're saving the file
      if File.basename(file) =~ /Untitled(\d+)?\.rb/ || save_as
        # It's an Untitled document, or save as action, prompt the user for the filename
        dialog = Gtk::FileChooserDialog.new("Save File As...",
                          @main.builder["window1"],
                          Gtk::FileChooser::ACTION_SAVE,
                          nil,
                          [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                          [Gtk::Stock::SAVE, Gtk::Dialog::RESPONSE_ACCEPT])
        if !@main.project.nil? && !@main.projview.get_selected_path.nil?
          if File.file? @main.projview.get_selected_path
            dialog.current_folder = File.dirname(@main.projview.get_selected_path)
          else
            dialog.current_folder = @main.projview.get_selected_path
          end
        else
          dialog.current_folder = File.dirname(file)
        end
        dialog.current_name = File.basename(file)
        resp = dialog.run
        dialog.hide
        if resp == Gtk::Dialog::RESPONSE_ACCEPT
          @untitled -= 1 if File.basename(file) =~ /Untitled(\d+)?.rb/
          @pages[file][:doc].write_to_disk(dialog.filename)
          @pages[dialog.filename] = @pages.delete(file)
          return true
        end
        return false
      else
        @pages[file][:doc].write_to_disk()
      end
    end
  end

  def get_current()
    tab = @notebook.get_nth_page(@notebook.page)
    document = nil
    @pages.each do |file,gtk|
      if gtk[:tab] == tab
        document = file
        break
      end
    end
    return document
  end
  
  def get_current_doc()
    doc = get_current()
    return @pages[doc][:doc]
  end

  def save_current(save_as = false)
    document = get_current()
    return if document.nil?
    save_document(document,save_as)
  end

  def try_to_save_all()
    @pages.each do |file,gtk|
      if File.basename(file) =~ /Untitled(\d+)?\.rb/ && gtk[:doc].buffer.modified?
        ret = save_document(file)
      else
        ret = true
      end
      return ret unless ret
    end
  end
  
  def jump_to(line)
    return if line.nil?
    if not switch_to(line[:path])
      load_tab(line[:path])
    end
    @pages[line[:path]][:doc].jump_to_line(line[:line] - 1, line[:search_str])
  end
end
