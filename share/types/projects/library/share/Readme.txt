This folder will hold the resources you will need for your library, such as images, and config
files.  If your library does not need this, then feel free to remove the folder.
