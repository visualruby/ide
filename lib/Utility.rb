module VR
  module IDE
    class << self
      def get_project_folder()
        tests = [ ]
        if ENV["HOMEDRIVE"].nil? && ENV["HOMEPATH"].nil?
          tests << File.join(ENV["HOME"],"Documents","Projects")
          tests << File.join(ENV["HOME"],"Projects")
          tests << ENV["HOME"]
        else
          tests << File.join(ENV["HOMEDRIVE"],ENV["HOMEPATH"],"Documents","Projects")
          tests << File.join(ENV["HOMEDRIVE"],ENV["HOMEPATH"],"Projects")
          tests << File.join(ENV["HOMEDRIVE"],ENV["HOMEPATH"],"My Documents","Projects")
          tests << File.join(ENV["HOMEDRIVE"],ENV["HOMEPATH"])
        end
        
        tests.each do |x|
          if File.exists? x
            return x
          end
        end
        return Dir.pwd # Should never reach here, but just in-case.
      end
    end
  end
end