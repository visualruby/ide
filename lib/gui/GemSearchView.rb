class GemSearchView < VR::ListView
  def initialize()
    hash = {:gems => {:pix => Gdk::Pixbuf, :name => String}, :version => String, :platform => String, :description => String, :website => String, :tuple => Array}
    super(hash)
    col_visible(:tuple => false)
    self.enable_grid_lines = Gtk::TreeView::GridLines::BOTH
  end
end