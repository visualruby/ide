class SettingsDialog
  include GladeGUI
  
  def show(parent)
    @main = parent
    @api = RubyGemsAPI.new
    load_glade(__FILE__,parent)
    
    VR::IDE::Settings[:general].each do |k,v|
      @builder[k.to_s].text = v
    end
    
    VR::IDE::Settings[:editor].each do |k,v|
      if @builder[k.to_s].class == Gtk::CheckButton
        @builder[k.to_s].active = v
      elsif @builder[k.to_s].class == Gtk::Entry
        @builder[k.to_s].text = v.to_s
      end
    end
    
    @builder["style_holder"].add(@editor_style = Gtk::ComboBox.new(true))
    @builder["logins_holder"].add(@logins = VR::ListView.new({:login => String, :api_key => String}))
    
    set_glade_all(self)
    
    @gsssm = Gtk::SourceStyleSchemeManager.new
    lm = Gtk::SourceLanguageManager.new
    
    @gsssm.scheme_ids.each do |scheme|
      @editor_style.append_text(scheme)
    end
    
    @editor_style.active = @gsssm.scheme_ids.index(VR::IDE::Settings[:editor][:style_scheme])
    
    @builder["style_display"].buffer = Gtk::SourceBuffer.new
    @builder["style_display"].buffer.language = lm.get_language("ruby")
    @builder["style_display"].buffer.highlight_syntax = true
    @builder["style_display"].buffer.text =<<-EOD
# This is a Comment
require 'some_lib'

class Hello
  def initialize
    @prop = "Hello World"
  end

  def hello
    puts @prop
  end
end
Hello.new.hello
EOD
    
    VR::IDE::Settings[:rubygems][:logins].each do |k,v|
      @logins.add_row({:login => k, :api_key => v})
    end
    
    @builder["password"].visibility = false
    
    VR::IDE::Settings[:editor].each_key do |k|
      if @builder[k.to_s].class == Gtk::CheckButton
        @builder[k.to_s].signal_connect("toggled") {|*x| setting_changed(*x)}
      elsif @builder[k.to_s].class == Gtk::Entry
        @builder[k.to_s].signal_connect("changed") {|*x| setting_changed(*x)}
      elsif @builder[k.to_s].nil?
        @editor_style.signal_connect("changed") {|*x| setting_changed(*x)}
      end
    end
    
    apply_preview()
    
    show_window()
  end
  
  def apply_preview()
    VR::IDE::Settings[:editor].each do |k,v|
      if k == :style_scheme
        @builder["style_display"].buffer.__send__((k.to_s + "=").to_sym,@gsssm.get_scheme(v))
      else
        @builder["style_display"].__send__((k.to_s + "=").to_sym,v)
      end
    end
  end
  
  def browseGlade__clicked(*args)
    dlg = Gtk::FileChooserDialog.new("Locate Glade Executable",
            @builder["window1"],
            Gtk::FileChooser::ACTION_OPEN,
            nil,
            [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
            [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    if dlg.run == Gtk::Dialog::RESPONSE_ACCEPT
      @builder["glade"].text = dlg.filename
    end
    dlg.destroy
  end
  
  def browseWeb__clicked(*args)
    dlg = Gtk::FileChooserDialog.new("Locate Glade Executable",
            @builder["window1"],
            Gtk::FileChooser::ACTION_OPEN,
            nil,
            [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
            [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
    if dlg.run == Gtk::Dialog::RESPONSE_ACCEPT
      @builder["browser"].text = dlg.filename
    end
    dlg.destroy
  end
  
  def setting_changed(control,*args)
    bn = control.builder_name
    bn = "style_scheme" if bn.nil?
    bn = bn.to_sym
    
    if control.class == Gtk::CheckButton
      VR::IDE::Settings[:editor][bn] = control.active?
    elsif control.class == Gtk::Entry
      if VR::IDE::Settings[:editor][bn].class == Fixnum
        VR::IDE::Settings[:editor][bn] = control.text.to_i
      elsif VR::IDE::Settings[:editor][bn].class == String
        VR::IDE::Settings[:editor][bn] = control.text
      end
    elsif control.class == Gtk::ComboBox
      VR::IDE::Settings[:editor][bn] = control.active_text
    end
    apply_preview
  end
  
  def logins__row_activated(*args)
    row = @logins.selected_rows()[0]
    @builder["username"] = row[:login]
  end
  
  def save__clicked(*args)
    username = @builder["username"].text
    password = @builder["password"].text
    key = @api.setup_key(username,password)
    return if key.nil?
    VR::IDE::Settings[:rubygems][:logins][username] = key
    @logins.model.clear
    VR::IDE::Settings[:rubygems][:logins].each do |login,key|
      @logins.add_row(:login => login, :api_key => key)
    end
  end
  
  def apply__clicked(*args)
    VR::IDE::Settings[:general].each_key do |k|
      VR::IDE::Settings[:general][k] = @builder[k.to_s].text
    end
    
    VR::IDE::Settings[:editor].each_key do |k|
      if @builder[k.to_s].class == Gtk::CheckButton
        VR::IDE::Settings[:editor][k] = @builder[k.to_s].active?
      elsif @builder[k.to_s].class == Gtk::Entry
        if VR::IDE::Settings[:editor][k].class == Fixnum
          VR::IDE::Settings[:editor][k] = @builder[k.to_s].text.to_i
        elsif VR::IDE::Settings[:editor][k].class == String
          VR::IDE::Settings[:editor][k] = @builder[k.to_s].text
        end
      elsif @builder[k.to_s].nil?
        VR::IDE::Settings[:editor][k] = @editor_style.active_text
      end
    end
    
    VR::IDE::Settings.save
    @main.doc_manager.each_doc do |doc|
      doc.update_settings()
    end
  end
  
  def close__clicked(*args)
    VR::IDE::Settings.load
    destroy_window()
  end
end