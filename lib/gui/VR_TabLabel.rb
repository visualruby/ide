
class VR_TabLabel < Gtk::Frame
  type_register

  signal_new("close_clicked",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      Gtk::Frame)

  def initialize(icon,text,show_close=true)
    super()
    self.shadow_type = Gtk::ShadowType::NONE
    hbox = Gtk::HBox.new
    if icon.class == String && icon.empty?
      icon = Gdk::Pixbuf.new(icon)
    end
    if icon.class == Gdk::Pixbuf
      hbox.pack_start(@icon = Gtk::Image.new(icon),true,true,2)
    end
    hbox.pack_start(@text = Gtk::Label.new(),true,true,2)
    if show_close
      hbox.pack_start(@close = Gtk::EventBox.new(),false,false,2)
      @close.add(Gtk::Image.new(Gtk::Stock::CLOSE,Gtk::IconSize::SMALL_TOOLBAR))
    end
    @text.text = text
    self.add(hbox)
    if show_close
      @close.signal_connect("button_release_event") do |widget|
        self.signal_emit("close_clicked",self)
      end
    end
    self.show_all
  end

  def signal_do_close_clicked(widget)
      # Nothing to do here, up to the user to work with this
      ""
  end

  def text
    return @text.text
  end

  def text=(value)
    @text.text = value
  end
end
