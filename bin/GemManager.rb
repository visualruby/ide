
class GemManager < GLib::Object

  type_register()
  
  signal_new("progress_msg",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    String)
    
  signal_new("progress_pulse",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    String)

  signal_new("progress_set",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    Fixnum)
    
  signal_new("progress_update",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    Fixnum)
    
  signal_new("search_done",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    String)
    
  signal_new("dep_check_done",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    String)
    
  signal_new("loading_done",
    GLib::Signal::RUN_FIRST,
    nil,
    nil,
    String)

  include GladeGUI
  THE_PLATFORM = Gem::Platform.local.cpu + "-" + Gem::Platform.local.os
  
  def show(parent)
    load_glade(__FILE__,parent)
    @main = parent
    
    @builder["geminstallbox"].add(@install = GemView.new)
    @builder["gemownbox"].add(@owned = GemTree.new)
    @builder["gemsearchbox"].add(@search = GemSearchView.new)
    @GEM_PIX = Gdk::Pixbuf.new(File.join($THE_PATH,"share","icons","rubygems.png"),16,16)
    @api = RubyGemsAPI.new
    set_glade_all(self)
    self.status = "Loading Gem information...."
    @max = 0
    
    toggle_usable(false)
    Thread.new do
      sleep(0.5)
      self.signal_emit("progress_msg","Processing local gems...")
      populate_installed()
      self.signal_emit("progress_msg","Processing remote gems...")
      populate_owned()
      self.signal_emit("progress_msg","Ready.")
      self.signal_emit("progress_set",0)
      self.signal_emit("loading_done","")
    end
    
    @builder["install_gem"].sensitive = false
    
    @search.signal_connect("cursor-changed") {|*args| search__cursor_changed(*args)}

    show_window()
  end
  
  def populate_installed
    @install.model.clear
    theEnum = Gem::Specification.find_all {|x| true }
    self.signal_emit("progress_set",theEnum.count)
    theEnum.each_with_index do |spec,i|
      name = spec.name
      version = spec.version.version
      author = spec.author
      website = spec.homepage
      @install.add_row(:pix => @GEM_PIX, :name => name, :version => version, :author => author, :website => website)
      self.signal_emit("progress_update",i)
    end
  end
  
  def populate_owned
    @owned.model.clear
    return unless @api.has_key?
    self.signal_emit("progress_pulse","")
    @gems = @api.get_obj_url("/api/v1/gems.yaml")
    @gems.each do |gem,i|
      iter = @owned.add_row(nil,:pix=>@GEM_PIX, :name => gem["name"], :version => "", :author => gem["authors"], :downloads => 0, :website => gem["homepage_uri"])
      total_downloads = 0 
      gem_info = @api.get_obj_url("/api/v1/versions/#{gem['name']}.yaml")
      gem_info.each do |info|
        @owned.add_row(iter,:pix=>@GEM_PIX, :name => gem["name"], :version => info["number"] + "-" + info["platform"], :author => "", :downloads => info["downloads_count"], :website => "")
        total_downloads += info["downloads_count"]
      end
      @owned.update_iter(iter,:downloads,total_downloads)
    end
    self.signal_emit("progress_pulse","")
  end
  
  def search_gem_clear__clicked(*args)
    @gem_name = ""
    set_glade_all()
    @search.model.clear
    @builder["install_gem"].sensitive = false
  end
  
  def gem_search__clicked(*args)
    @gem_name = @gem_version = ""
    @complete = @released = @prerelease = @lastest = false
    get_glade_all()
    if @gem_name.empty?
      VR::Dialog.message_box("You must specify a Gem name to search for!","VisualRuby Gem Search")
      return
    end
    
    @search.model.clear
    toggle_usable(false)
    self.status = "Searching for #{@gem_name}..."
    
    Thread.new do
      self.signal_emit("progress_pulse","")
      
      fetcher = Gem::SpecFetcher.fetcher
      
      @type = if @complete
        :complete
      elsif @released
        :released
      elsif @prerelease
        :prerelease
      else
        :latest
      end
      
      spec_tuples = fetcher.detect(@type) do |name_tuple|
        name_tuple.name =~ /#{@gem_name}/
      end
      
      spec_tuples.sort! do |x,y|
        x[0].name <=> y[0].name
      end
      
      spec_tuples = spec_tuples.map {|st|
        nt, s = st
        spec = s.fetch_spec(nt)
        [nt,s,spec]
      }
      
      spec_tuples.each do |tuple|
        show_gem = false
        if @complete
          show_gem = true
        else
          if tuple[0].platform != THE_PLATFORM && tuple[0].platform != "ruby"
            show_gem = false
          else
            show_gem = true
          end
        end
        @search.add_row(
          :pix => @GEM_PIX,
          :name => tuple[0].name,
          :version => tuple[0].version.version,
          :platform => tuple[0].platform,
          :description => tuple[2].description,
          :website => tuple[2].homepage,
          :tuple => tuple) if show_gem
      end
      self.signal_emit("progress_set",0)
      self.signal_emit("search_done","")
    end
    @started = Time.now
  end
  
  def close__clicked(*args)
    destroy_window()
  end
  
  def on_window1_delete_event(*args)
    destroy_window()
  end
  
  def search__cursor_changed(*args)
    iter = @search.selection.selected
    if iter.nil?
      @builder["install_gem"].sensitive = false
    else
      @builder["install_gem"].sensitive = true
    end
  end
  
  def install_gem__clicked(*args)
    iter = @search.selection.selected
    row = @search.vr_row(iter)
    if row[:platform] != THE_PLATFORM && row[:platform] != "ruby"
      return unless VR::Dialog.yesno("This gem isn't designed for your platform, do you wish to still install it?","VisualRuby - Gem Install")
    end
    
    VR::Thread.protect do
      @the_specs = [row[:tuple]]
      toggle_usable(false)
      @builder["install_gem"].sensitive = false
      self.status = "Gathering dependencies for #{row[:tuple][0].name}..."
      self.signal_emit("progress_pulse","")
    end
    
    Thread.new do
      fetcher = Gem::SpecFetcher.fetcher
      # Check for Dependencies
      di = Gem::DependencyInstaller.new
      di.find_spec_by_name_and_version(row[:tuple][0].name,Gem::Requirement.new(row[:tuple][0].version))
      
      specs = di.gather_dependencies
      
      if specs.length > 1
        the_specs = specs.collect do |x|
          st = fetcher.detect(:complete) do |nt|
            nt === x.name_tuple
          end
          st[0]
        end
      end
      the_specs = [] if the_specs.nil?
      the_specs = the_specs.collect {|x| x == row[:tuple] ? nil : x }.compact
      the_specs << row[:tuple]
      VR::Thread.protect do
        @the_specs = the_specs
        self.signal_emit("progress_pulse","")
        self.signal_emit("dep_check_done","")
      end
    end
  end
  
  def gem_update__clicked(*args)
    VR::Thread.new do
      self.signal_emit("progress_pulse","")
      fetcher = Gem::SpecFetcher.fetcher
      hig = {}
      
      theEnum = Gem::Specification.find_all {|x| true}
      theEnum.each do |spec|
        if hig[spec.name].nil? or hig[spec.name].version < spec.version then
          hig[spec.name] = spec
        end
      end
      
      result = []
      hig.each do |l_name, l_spec|
        dependency = Gem::Dependency.new l_spec.name, "> #{l_spec.version}"
        
        spec_tuples, _ = fetcher.search_for_dependency dependency
        matching_gems = spec_tuples.select do |g,_|
          g.name == l_name and g.match_platform?
        end
        
        highest_remote_gem = matching_gems.sort_by { |g,_| g.version }.last
        
        highest_remote_gem ||= [Gem::NameTuple.null]
        highest_remote_ver = highest_remote_gem.first.version
        
        if (l_spec.version < highest_remote_ver)
          result << [l_spec.name, l_spec.version, [l_spec.version, highest_remote_ver].max]
        end
      end
      
      VR::Thread.protect do
        self.signal_emit("progress_pulse","")
        self.signal_emit("progress_set",0)
        if result.empty?
          VR::Dialog.message_box("No updates found!")
        else
          res = VR::Dialog.listbox("The following #{result.count} updates have been found, do you wish to update?",
            result.collect {|x| "#{x[0]} (#{x[1]} => #{x[2]})" })
          if res
            
          end
        end
      end
    end
    
  end
  
  #region Private Methods
  private
  
  def toggle_usable(use = true)
    @builder["search_gem_clear"].sensitive = use
    @builder["gem_search"].sensitive = use
    @builder["GemManager.gem_name"].sensitive = use
    @builder["GemManager.released"].sensitive = use
    @builder["GemManager.prerelease"].sensitive = use
    @builder["GemManager.latest"].sensitive = use
    @builder["GemManager.complete"].sensitive = use
    @search.sensitive = use
  end
  
  def start_pulse()
    @pulse = true
    GLib::Timeout.add(100) do
      @builder["progress"].pulse if @pulse
      @pulse
    end
  end
  
  def stop_pulse()
    @pulse = false
  end
  
  def status=(text)
    @builder["status"].text = text
  end
  
  def progress=(percent)
    @builder["progress"].fraction = (percent.is_a? Float) ? percent : percent * 0.01
  end
  #endregion
  
  #region Custom Signals for use in Threads
  def signal_do_progress_msg(msg)
    VR::Thread.protect do
      self.status = msg
    end
  end
  
  def signal_do_progress_pulse(str)
    VR::Thread.protect do
      if @pulse
        stop_pulse()
      else
        start_pulse()
      end
    end
  end
  
  def signal_do_progress_set(max)
    VR::Thread.protect do
      stop_pulse() if @pulse
      self.progress = 0.0
      @max = max
    end
  end
  
  def signal_do_progress_update(step)
    VR::Thread.protect do
      self.progress = ((step * 100) / @max)
    end
  end
  
  def signal_do_search_done(blah)
    VR::Thread.protect do
      toggle_usable
      self.progress = 0.0
      self.status = "Search completed. Completed in #{"%.2f" % (Time.now - @started)} secs"
    end
  end
  
  def signal_do_loading_done(blah)
    VR::Thread.protect do
      toggle_usable
      self.progress = 0.0
    end
  end
  
  def signal_do_dep_check_done(blah)
    VR::Thread.protect do
      self.progress = 0.0
      self.status = "Collection of dependencies completed."
      if @the_specs.length > 1
        the_list = @the_specs[0..-2].collect {|x| x[0].name + "-" + x[0].version.version}
        the_gem = @the_specs[-1][0].name + "-" + @the_specs[-1][0].version.version
        if VR::Dialog.listbox("The gem #{the_gem} has the following dependencies that need\nto be installed, continue with installation?",
              the_list)
          mngr = GemDialog.new(@the_specs)
          mngr.show(self)
          toggle_usable
          @builder["install_gem"].sensitive = true
        else
          toggle_usable
          @builder["install_gem"].sensitive = true
        end
      else
        mngr = GemDialog.new(@the_specs)
        mngr.show(self)
        toggle_usable
        @builder["install_gem"].sensitive = true
      end
    end
  end
  #endregion
end