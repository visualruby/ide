require 'gtk2'
require 'open3'
require '../lib/Process'
require '../lib/gui/VR_TextViewCommon'
require '../lib/gui/VR_PseudoTerminal'

STDOUT.sync = true
STDERR.sync = true

win = Gtk::Window.new
sw = Gtk::ScrolledWindow.new
tv = VR_PseudoTerminal.new(nil)
vbox = Gtk::VBox.new
btn = Gtk::Button.new("Execute test_shell.rb")

sw.add(tv)
vbox.pack_start(sw,true,true)
vbox.pack_start(btn,false,false)
win.add(vbox)

win.set_default_size(400,300)

proc = VR::Process.new(["ruby","test_shell.rb"])

proc.signal_connect("stdout") do |lproc, data|
  tv.append_text(data)
end

proc.signal_connect("stderr") do |lproc, data|
  tv.append_error(data)
end

proc.signal_connect("exited") do |lproc|
  tv.append_text("Process #{lproc.pid} exited with status #{lproc.exitstatus}")
  btn.sensitive = true
end

btn.signal_connect("clicked") do
  btn.sensitive = false
  proc.pexecute
end

win.signal_connect("destroy") do
  Gtk.main_quit
end

win.show_all()

Gtk.main