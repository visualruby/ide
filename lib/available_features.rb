AVAILABLE_FEATURES = Hash.new

# Version control libraries.  Some may or may not be available
begin
  require 'mercurial-ruby'
  AVAILABLE_FEATURES[:hg] = true
rescue LoadError
  AVAILABLE_FEATURES[:hg] = false
end

begin
  require 'git'
  AVAILABLE_FEATURES[:git] = true
rescue LoadError
  AVAILABLE_FEATURES[:git] = false
end

begin
  require 'svn_wc'
  AVAILABLE_FEATURES[:svn] = true
rescue LoadError
  AVAILABLE_FEATURES[:svn] = false
end

# WebKit may or may not be available for use in our Program.  Will have to wait and see.
begin
  require 'webkit'
  AVAILABLE_FEATURES[:webkit] = true
rescue LoadError
  AVAILABLE_FEATURES[:webkit] = false
end

# VTE is only available on Unix based systems, as Terminal Functions are not emulatable on Windows.
begin
  require 'vte'
  AVAILABLE_FEATURES[:vte] = true
rescue LoadError
  AVAILABLE_FEATURES[:vte] = false
end
