class VR::GemDownloadReporter < VR::ListView
  type_register
  
  signal_new("progress",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      Fixnum)
      
  signal_new("append_row",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      Hash)
      
  signal_new("download_done",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      String)
      
  attr_reader :cl
  def initialize(*args)
    cols = {:gem => {:pix => Gdk::Pixbuf, :name => String}, :size => String, :progress => VR::ProgressCol}
    super(cols)
    self.headers_visible = true
    @pix = Gdk::Pixbuf.new(File.join($THE_PATH,"share","icons","rubygems.png"),16,16)
  end
  
  def fetch(filename, filesize)
    @filename = filename
    @size = filesize.to_i
    @progress = 0
    @units = @size.zero? ? 'B' : '%'
    @cl = {
             :pix => @pix, :name => filename,
             :size => @units == 'B' ? calc_size_string : "Unknown",
             :progress => 0
    }
    self.signal_emit("append_row",@cl)
  end
  
  def update(bytes)
    theProgress = if @units == 'B' then
                    bytes
                  else
                    ((bytes.to_f * 100) / @size.to_f).ceil
                  end
    
    return if theProgress == @progress
    
    signal_emit("progress",theProgress)
  end
  
  def done
    signal_emit("progress",@units == '%' ? 100 : @progress )
    signal_emit("download_done",@filename)
  end
  
  private
  
  def calc_size_string
    if @size < 1024 # Bytes
      return "%dB" % @size
    elsif @size > 1024 # KBytes
      return "%0.2fKB" % @size / 1024.0
    elsif @size > 1024 * 1024 # MBytes
      return "%0.2fMB" % @size / (1024 * 1024).to_f
    else # GBytes
      return "%0.2fGB" % @size / (1024 * 1024 * 1024).to_f
    end
  end
  
  def signal_do_append_row(row)
    VR::Thread.protect do
      @row = add_row()
      row.each_pair {|k,v| @row[k] = v }
      scroll_to_cell(@row.path,column(:pix),false,0.0,0.0)
    end
  end
  
  def signal_do_progress(data)
    VR::Thread.protect do
      @progress = data
      @row[:progress] = @units == 'B' ? ((@progress.to_f * 100) / @size.to_f).ceil * 0.01 : @progress
    end
  end
  
  def signal_do_download_done(file)
  end
end