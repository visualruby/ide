class GtkProgressReporter < Gtk::ProgressBar
  def initialize(out_stream, size, initial_message, terminal_message = "done")
    super()
    self.show_text = true
    self.text_xalign = 0.5
    self.text_yalign = 0.5
    self.text = "#{initial_message} 0%"
    @size = size
    @count = 0
    @final = terminal_message
  end
  
  def updated(message)
    per = @size * 100 / @count
    fper = per * 0.01
    self.text = "#{message} #{"%d" % per}%"
    self.fraction = fper
  end
  
  def done
    self.text = @final
    self.fraction = 1.0
  end
end