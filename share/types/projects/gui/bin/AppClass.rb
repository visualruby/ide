class <%= app_class_name %>

  include GladeGUI

  def show()
    # Load Glade file from glade/<%= app_class_name %> into @builder
    load_glade(__FILE__)
    
    # Intialize any variables here for the Glade Controls, (i.e. <%= app_class_name %>.label1)
    @label1 = "Hello World!"
    
    # Populate Glade controls with instance variables (i.e. <%= app_class_name %>.label1)
    set_glade_all()
    
    # Finalize any tweeks to Glade Controls that should be done programatically,
    # such as sensitivity, programmatically created controls, and such.
    
    # Show the window, with all the widgets currently created
    show_window() 
  end
  
  # Define Event Handlers, and Custom Methods for use in the application here.
  
end