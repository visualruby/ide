class Gem::GtkUI
  def initialize()
  end
  
  def set_console(edit)
    @edit = edit
  end
  
  def backtrace exception
    say "Exception: #{exception.class}"
    say "Message: #{exception.message}"
    say "Backtrace:"
    say exception.backtrace.join("\n")
  end
  
  def choose_from_list(question, list)
    say "Question: #{question}"
    say "List: #{list}"
  end
  
  def ask_yes_no(question, default=nil)
    say "Question: #{question}"
    say "Default: #{default}"
    default
  end
  
  def ask(question)
    say "Question: #{question}"
  end
  
  def ask_for_password(question)
    return VR::Dialog.userpass(question)
  end
  
  def say(statement="")
    @edit.append_text("#{statement}\n")
  end
  
  def alert(statement,question=nil)
    say "Alert> #{statement}"
    say "Question> #{question}"
  end
  
  def alert_warning(statement, question=nil)
    say "Alert Warning> #{statement}"
    say "Question> #{question}"
  end
  
  def alert_error(statement, question=nil)
    say "Alert Error> #{statement}"
    say "Question> #{question}"
  end
  
  def debug(statement)
    say "Debug> #{statement}"
  end
  
  def terminate_interaction(status = 0)
    say "Terminate Interaction, status #{status}"
    raise Gem::SystemExitException, status
  end
  
  def progress_reporter(*args)
    puts "Progress Reporter made"
    @prep ||= GtkProgressReporter.new(*args)
  end
  
  def download_reporter(*args)
    @drep ||= VR::GemDownloadReporter.new(*args)
  end
end