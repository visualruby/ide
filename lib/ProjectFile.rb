class ProjectFile
	attr_accessor :name, :path, :options, :last_session

	class << self
		def load(fname)
			fname = File.expand_path(fname)
			pf = ProjectFile.new
			data = YAML::load(IO.read(fname))
			pf.name = data[:name]
			pf.options.merge!(data[:options])
			pf.last_session.merge!(data[:last_session])
			# Correct Path information, incase of moving the folder to a different place.
			if data[:path] != File.dirname(fname)
				data[:last_session][:tree].each_with_index do |x,i|
					pf.last_session[:tree][i][data[:path]] = File.dirname(fname)
				end
				data[:last_session][:open].each_with_index do |x,i|
					pf.last_session[:open][i][data[:path]] = File.dirname(fname)
				end
				pf.last_session[:current_file][data[:path]] = File.dirname(fname) unless pf.last_session[:current_file].nil?
			end
			pf.path = File.dirname(fname)
			pf
		end
	end

	def initialize()
		@name = nil
		@path = nil
		@options = {}
		@last_session = {:tree=>[],:open=>[],:current_file=>""}
	end
	
	def filename
	  return File.join(@path,@name + ".rbproj")
	end

	def save
		return if @path.nil? or @name.nil?
		File.open(filename,"w") do |fh|
			fh.print YAML::dump({:name=>@name,:options=>@options,:path=>@path,:last_session=>@last_session})
		end
	end

	def [](*fpath)
		File.join(@path,fpath)
	end

	def enum_all()
		Dir.glob(File.join(@path,"**","*")) do |x|
			if File.directory? x
				x[@path + "/"] = ""
				yield :dir, x
			elsif File.file? x
				x[@path + "/"] = ""
				yield :file, x
			end
		end
	end

	def enum_folders()
		enum_all do |type, x|
			yield x if type == :dir
		end
	end

	def enum_files()
		enum_all do |type, x|
			yield x if type == :file
		end
	end

	def files
		lfiles = []
		enum_all do |type,x|
			lfiles << x if type == :file
		end
		lfiles
	end

	def folders
		ldirs = []
		enum_all do |type,x|
			ldirs << x if type == :dir
		end
	end

	def files_folders
		all = []
		enum_all do |type,x|
			all << x
		end
		all
	end
end
