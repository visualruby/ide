class GemTree < VR::TreeView
  def initialize
    hash = { :gem => {:pix => Gdk::Pixbuf, :name => String }, :version => String, :downloads => Fixnum, :author => String, :website => String }
    super(hash)
    col_visible(:version => true, :version => true, :author => true, :downloads => true, :website => true)
    self.headers_visible = true
  end
  
  def add_row(parent, columns)
    child = self.model.append(parent)
    columns.each do |col, val|
      puts "Column #{col}: is nil!" if val.nil?
      puts "Column Info: \n#{columns.inspect}" if val.nil?
      child[id(col)] = val
    end
    return child
  end
  
  def update_iter(iter,column,value)
    iter[id(column)] = value
  end
end