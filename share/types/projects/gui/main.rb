#!/usr/bin/ruby

# The Main Require to allow for VisualRuby to operate properly in the sub-program
require 'vrlib'

# Require any stdlib or rubygems libraries here.


#make program output in real time so errors visible in VR.
STDOUT.sync = true
STDERR.sync = true

#everything in these directories will be included
$THE_PATH = File.expand_path(File.dirname(__FILE__))
require_all Dir.glob($THE_PATH + "/bin/**/*.rb") 

# Create the Main Instance, and show the window, beginning the program execution
<%= app_class_name %>.new.show
