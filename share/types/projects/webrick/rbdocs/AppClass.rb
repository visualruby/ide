class <%= app_class_name %> < WEBrick::HTTPServlet::AbstractServlet
  def do_GET(req, resp)
    resp["Content-Type"] = "text/html"
    resp.status = 200
    resp.body = "<html><head><title>Servlet</title></head><body><center><h1>Hello from #{self.class.to_s}</h1></center></body></html>"
  end

  alias_method :do_POST, :do_GET
end