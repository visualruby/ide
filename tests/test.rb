require 'open3'
require 'thread'

STDOUT.sync = true
STDERR.sync = true

stdin, stdout, stderr, thread = Open3.popen3("ruby test_shell.rb")
pid = thread.pid

Thread.abort_on_exception = true

puts "Process started with PID: #{pid}"

Thread.new do
  loop do
    rs, ws, es = IO.select([stdout],[stdin],[stderr],0.5)
    if rs[0] == stdout
      data = ""
      while c = stdout.getc
        if c == "\n"
          puts "stdout(#{pid}): #{data}"
          data = ""
        elsif c == "\r"
          # Ignore this, as it's used on Windows, and Mac OS X
        else
          data += c
        end
      end
    end
    if es[0] == stderr
      data = ""
      while c = stdout.getc
        if c == "\n"
          puts "stdout(#{pid}): #{data}"
          data = ""
        elsif c == "\r"
          # Ignore this, as it's used on Windows and Mac OS X
        else
          data += c
        end
      end
    end
    begin
      Process.kill(0,pid)
    rescue Errno::ESRCH
      break
    end
  end
end

puts "Process exited with status: #{thread.value}"
