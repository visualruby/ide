class GemView < VR::ListView
  def initialize()
    hash = {:gems => {:pix => Gdk::Pixbuf, :name => String}, :version => String, :author => String, :website => String }
    super(hash)
    self.enable_grid_lines = Gtk::TreeView::GridLines::BOTH
    
  end
end