
class FindDialog

  include GladeGUI
  include VR::IDE::SearchDialog
  
  def show(parent)
    load_glade(__FILE__,parent)
    @main = parent
    
    set_glade_all(self)
    
    init_search()
    @search.signal_connect("changed") { toggle_find() }
    toggle_find()
    @main.doc_manager.get_current_doc.reset_find()
    
    show_window()
  end
  
  def on_window1_delete_event()
    destroy_window()
  end
end