## RubyVM::InstructionSequence#to_a  Returns A 14 element array, and sub-arrays defining the code

# Positions in the Array Structure:
MAGIC = 0       # A string identifying the data format. Always YARVInstructionSequence/SimpleDataFormat
MAJOR_VERSION = 1 # The major version of the instruction sequence
MINOR_VERSION = 2 # The minor version of the instruction sequence
FORMAT_TYPE = 3 # A number identifying the data format. Always 1
MISC = 4 # A Hash Containing: :arg_size, :local_size, :stack_max
LABEL = 5 # The name of the context (block, method, class, module, etc) that the instruction belongs to
PATH = 6 # The relative path to the Ruby file where the instruction sequence was loaded from
ABSOLUTE_PATH = 7 # The absolute path to the Ruby file where the instruction sequence was loaded from
FIRST_LINENO = 8 # The number of the first source line where the instruction sequence was loaded from
TYPE = 9 # The Type of instruction sequence.  Valid values are: :top, :method, :block, :class, :rescue, :ensure, :eval, :main, and :define_guard
LOCALS = 10 # An array containing the names of all arguments and local variables as symbols
ARGS = 11 # The arity if the method or block only has required arguments
CATCH_TABLE = 12 # A list of exceptions and control flow operators (rescue, next, redo, break, etc)
BYTECODE = 13 # An array of arrays containing the instruction names and operands that make up the body of the instruction sequence

OPERAND = 0 # The Operand to be used, eg: :defineclass, :putobject, :putiseq (For Methods, and blocks, equates to SBYTECODE)
SYMBOL = 1
SBYTECODE = 2

IS_A_MODULE = /module/  # bytecode[LABEL] ~= IS_A_MODULE when bytecode[OPERAND] == :defineclass