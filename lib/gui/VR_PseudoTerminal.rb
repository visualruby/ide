
class VR_PseudoTerminal < Gtk::TextView
  include VR_TextViewCommon
  
  def initialize(main)
    super()
    @lines = []
    @errors = []
    @main = main
    self.modify_base(Gtk::STATE_NORMAL,Gdk::Color.new(0,0,0))
    self.modify_text(Gtk::STATE_NORMAL,Gdk::Color.new(65535,65535,65535))
    self.wrap_mode = Gtk::TextTag::WRAP_WORD
    self.modify_font(Pango::FontDescription.new("Courier New, Monospace, 10"))
    buffer.create_tag("blue", { "foreground" => "#00FF00", "underline" => Pango::UNDERLINE_SINGLE })
    buffer.create_tag("hilight", { "background" => "#FFF0A0" })
    buffer.create_tag("hilight2", { "background" => "#629bcd" })
    buffer.create_tag("error", { "foreground" => "#EE0000" })
    
    signal_connect("button_release_event") { jump_to(line_at_cursor() + 1) }
    signal_connect("key_release_event") { jump_to(line_at_cursor() + 1) }
  end
  
  def append_error(text)
    sline = buffer.end_iter.line
    buffer.text += text
    apply_tag_to_line(sline,"error")
    @errors << sline
    jump_to_end
  end
  
  def append_text(text)
    buffer.text += text
    jump_to_end
  end
  
  def jump_to_end()
    iter = buffer.end_iter
    mark = buffer.get_mark("EOB")
    if mark.nil?
      mark = buffer.create_mark("EOB",iter,true)
    else
      buffer.move_mark(mark,iter)
    end
    scroll_to_mark(mark,0.0,true,1.0,0.0)
    buffer.place_cursor(iter)
    scroll_to_cursor
  end
  
  def clear()
    self.buffer.text = ""
    @lines = []
    @errors = []
  end
  
  def buffer__move_cursor
    remove_tag("hilight2")
    line = line_at_cursor()
    apply_tag_to_line(line,"hilight2")
  end
  
  def jump_to(line = @current_line+1)
    return false if line > @lines.size - 1
    @current_line = line
    remove_tag("hilight2")
    apply_tag_to_line(@current_line,"hilight2")
    @main.doc_manager.jump_to(@lines[@current_line]) unless @main.nil?
    return false
  end
  
  def load_lines()
    @lines = []
    first = nil
    reg = /^.*(#{@main.project.path}[\w+|\/]*)\/([\w|\.]+):(\d+):/
    reg_str = /Found:\s(.+)$/
    self.buffer.text.each_line do |l|
      m = reg.match(l)
      if m.nil?
        @lines << nil
      else
        m2 = reg_str.match(l)
        search_str = (m2.nil?) ? "" : m2[1]
        @lines << { :path => m[1] + "/" + m[2], :file_name => m[2], :line => m[3].to_i, :search_str => search_str }
        first ||= @lines.last
      end
    end
    return first
  end
  
  def hilight_links(jump_to_first=false)
    first_valid_line = load_lines()
    unless @errors.empty?
      @errors.each do |x|
        apply_tag_to_line(x,"error")
      end
    end
    (0..@lines.size - 1).each do |i|
      next if @lines[i].nil?
      apply_tag_to_line(i,"blue",@lines[i][:file_name] + ":" + @lines[i][:line].to_s)
    end
    @current_line = 0
    (@main.doc_manager.jump_to(first_valid_line) if jump_to_first and first_valid_line) unless @main.nil?
  end
  
end
