module VR
  class Process < GLib::Object
    type_register()
    
    signal_new("stdout",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      String)
    
    signal_new("stderr",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      String)
    
    signal_new("exited",
      GLib::Signal::RUN_FIRST,
      nil,
      nil,
      VR::Process)
      
    attr_reader :pid, :exitstatus
    
    def initialize(cmd, wd=Dir.pwd)
      super()
      @cmd = cmd
      sanitize_command
      @wd = wd
      @stopping = false
    end
    
    def execute(chdir=nil)
      popen(chdir)
    end
    
    def pexecute(chdir=nil)
      popen(chdir) do
        loop do
          run_io_check()
          break if @out.closed? || @err.closed?
        end
      end
    end
    
    private
    
    def sanitize_command
      @cmd.each_with_index do |arg,i|
        @cmd[i] = "\"#{arg}\"" if arg.index(" ")
      end
    end
    
    def popen(chdir = nil, &iocb)
      opts = {}
      unless chdir.nil?
        opts[:chdir] = chdir
      end
      @in, @out, @err, @thread = Open3::popen3(@cmd.join(" "),opts)
      
      @pid = @thread.pid
      
      ::Thread.new do
        @exitstatus = @thread.value
        self.signal_emit("exited",self)
      end
      
      ::Thread.new { iocb.call } unless iocb.nil?
    end
    
    def check_for_data()
      begin
        rs, _ = IO.select([@out,@err],nil,nil,0.5)
      rescue IOError
        raise IOError
      rescue Errno::EBADF
        raise IOError
      end
      
      if rs.nil? or rs.empty?
        return nil
      else
        return rs
      end
    end
    
    def get_lines(input,signal)
      data = ""
      begin
        loop do
          rs, _ = IO.select([input],nil,nil,0.01)
          break if rs.nil? or rs.empty?
          c = input.getc
          break if c.nil?
          if c == "\n"
            self.signal_emit(signal,data + "\n")
            data = ""
          elsif c == "\r"
          else
            data += c
          end
        end
      rescue IOError, Errno::EBADF
        if data != ""
          self.signal_emit(signal,data + "\n")
        end
        return
      end
    end
    
    def run_io_check()
      begin
        rs = check_for_data()
      rescue IOError
        rs = nil
      end
      unless rs.nil?
        if rs.index(@out)
          get_lines(@out,"stdout")
        end
        
        if rs.index(@err)
          get_lines(@err,"stderr")
        end
      end
    end
    
    def close_handles()
      begin; @in.close; rescue; end
      begin; @out.close; rescue; end
      begin; @err.close; rescue; end
    end
    
    def signal_do_stdout(data)
    end
    
    def signal_do_stderr(data)
    end
    
    def signal_do_exited(proc)
      @stopping = true
      unless @in.nil?
        run_io_check()
        close_handles()
      end
    end
  end
end