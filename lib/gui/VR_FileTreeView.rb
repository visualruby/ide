class VR_FileTreeView < VR::FileTreeView

  include GladeGUI

  def initialize(main,*args)
    super(*args)
    @main = main
    @builder = main.builder
    @monitor = VR_FileMonitor.new("")
    self.drag_to(self)
    parse_signals
  end

  def monitor__new_entry(monitor,file)
    self.insert(file)
  end

  def monitor__del_entry(monitor,file)
    iter = get_iter_from_path(file)
    self.model.remove(iter) if iter
  end

  def self__drag_drop(view, context, x, y, time)
    path, _ = self.get_path(x,y)
    iter = self.model.get_iter(path)
    unless iter == 0 or iter.nil?
      iter = get_iter_from_path(File.dirname(file_name(iter))) if !folder?(iter)
      data = get_selected_path
      oiter = get_iter_from_path(data)
      new = file_name(iter)
      npath = File.join(new,oiter[id(:file_name)])
      FileUtils.move(data,npath)
      self.insert(npath)
      self.model.remove(oiter)
    end
  end

  def self__row_activated(*args)
    return unless rows = self.selected_rows
    file_name = rows.first[:path]
    if File.extname(file_name) == ".glade"
      # Launch Glade (Or Internal GUI Editor when developed)
      proc = VR::Process.new([VR::IDE::Settings[:general][:glade],file_name],@main.project.path)
      begin
        proc.execute
      rescue
        dlg = Gtk::MessageDialog.new(
          nil,
          Gtk::Dialog::MODAL,
          Gtk::MessageDialog::ERROR,
          Gtk::MessageDialog::BUTTONS_YES_NO,
          "Failed to execute glade command with #{VR::IDE::Settings[:general][:glade]}, do you wish to look for it?")
        dlg.title = "VisualRuby Execution Error"
        dlg.show_all
        if dlg.run == Gtk::Dialog::RESPONSE_YES
          @main.VR_MenuPreferences__activate
        end
        dlg.destroy
      end
    elsif [".png",".jpeg",".jpg",".gif",".bmp"].index(File.extname(file_name))
      # Load the Image
      img = Gdk::Pixbuf.new(file_name)
      width = img.width
      height = img.height
      if (width > Gdk::Screen.default.width)
        width = Gdk::Screen.default.width
      elsif (width < 100)
        width = 100
      end
      if (height > Gdk::Screen.default.height)
        height = Gdk::Screen.default.height
      elsif (height < 100)
        height = 100
      end
      @main.builder["imageshow"].pixbuf = img
      @main.builder["imageview"].resize(width,height)
      @main.builder["imageview"].title = "Preview: " + File.basename(file_name)
      @main.builder["imageview"].show
    elsif [".ico",".icns"].index(File.extname(file_name))
      # Do nothing, we can't support preview of Windows Icon files
    else
      @main.doc_manager.add(file_name) if not File.directory?(file_name)
    end
  end
  
  def self__button_release_event(w, event)
    return unless path = get_selected_path() and event.button == 3
    @builder["pvm_vcs"].visible = false
    @builder["pvm_execute"].visible = false
    @builder["pvm_edit_glade"].visible = false
    @builder["pvm_gem"].visible = false
    
    @builder["pvm_vcs"].visible = true if File.file? path
    @builder["pvm_execute"].visible = true if File.extname(path) == ".rb" || File.extname(path) == ""
    @builder["pvm_edit_glade"].visible = true if File.extname(path) == ".glade"
    @builder["pvm_gem"].visible = true if File.extname(path) == ".gemspec" || File.extname(path) == ".gem"
    if File.extname(path) == ".gem"
      @builder["pvmg_build"].sensitive = false
      @builder["pvmg_install"].sensitive = true
      @builder["pvmg_push"].sensitive = true
    elsif File.extname(path) == ".gemspec"
      @builder["pvmg_build"].sensitive = true
      @builder["pvmg_install"].sensitive = false
      @builder["pvmg_push"].sensitive = false
    end
    @builder["projviewmenu"]
    @builder["projviewmenu"].popup(nil,nil,event.button,event.time)
  end

  def path
    @path
  end

  def path=(val)
    @monitor.stop unless @path.nil?
    @path = val
    self.refresh(val) unless val.nil?
    @monitor.path = val unless val.nil?
    @monitor.start unless val.nil?
  end
end
