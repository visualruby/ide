
class VR_Document < Gtk::SourceView

  include VR_TextViewCommon
  attr_accessor :full_path_file

  def initialize(full_path_file, title_label, main)
    @main = main
    super()
    @title = title_label
    reload_from_disk(full_path_file)
    signal_connect("visibility_notify_event") { scroll_to_cursor() }
    self.smart_home_end = Gtk::SourceView::SmartHomeEndType::BEFORE
    @@lm ||= Gtk::SourceLanguageManager.new
    @@gsssm ||= Gtk::SourceStyleSchemeManager.new
    update_settings()
    lang = get_language(full_path_file)
    buffer.language = lang
    buffer.highlight_syntax = true
    self.wrap_mode = Gtk::TextTag::WRAP_NONE
    buffer.create_tag("hilight",  { "background" => "#FFF0A0" })
    buffer.create_tag("hilight_red", { "background" => "#FFC0C0" })
    buffer.signal_connect("changed") { remove_tag("hilight") }
    #self.signal_connect("key-press") {|x| check_for_end() }
    update_style()
  end
  
  def update_settings()
    VR::IDE::Settings[:editor].each do |k,v|
      if k == :style_scheme
        self.buffer.__send__((k.to_s + "=").to_sym,@@gsssm.get_scheme(v))
      else
        self.__send__((k.to_s + "=").to_sym,v)
      end
    end
  end
  
  def check_for_end()
    if @check_iter.nil?
      @check_iter = buffer.cursor_position()
      return
    end
    
  end
  
  def get_language(fn)
    # The proper way to Get the language, is to check with Gtk::SourceLanguageManager
    # to see if we can detect the language.  Next, if that doesn't work, try to check
    # the SheBang, and if that fails, try our special case ruby files, such as erb
    # for parsable ruby files, if all above fails, return Ruby as the default.
    
    theLang = @@lm.guess_language(fn,nil)
    return theLang unless theLang.nil?

    data = IO.read(fn)

    # Do we have a SheBang at the begining of the file?
    if (data =~ /^#!/) == 0
      # Next, do we have a env execution?
      if data =~ /\/\w+{0}env/
        # The next word, determins the interpreter
        md = /\/\w+{0}env\s+(?<interp>\w+)/.match(data)
        return @@lm.get_language(md[:interp]) unless md.nil?
      else
        md = /\/\w+{0}(?<interp>\w+\s)/.match(data)
        return @@lm.get_language(md[:interp]) unless md.nil?
      end
    end
    
    # Now we are at, checking for custom file extensions, such as gemspec, erb,
    # and such.
    case File.extname(fn)
      when ".gemspec"; return @@lm.get_language("ruby")
      when ".erb"; return @@lm.get_language("html")
    end
    
    # We've exhausted all options, default to Ruby
    return @@lm.get_language("ruby")
  end

  def update_style()
    modify_font(Pango::FontDescription.new("Monospace 10"))
    tab_array = Pango::TabArray.new(1,true)
    tab_array.set_tab(0,Pango::TAB_LEFT, 2 * 8)
    set_tabs(tab_array)
  end

  def reload_from_disk(fn = @full_path_file)
    @full_path_file = fn
    @title.text = File.basename(@full_path_file)
    buffer.text = File.open(@full_path_file,"r").read if File.file?(@full_path_file.to_s)
    buffer.modified = false
    @modified_time = mod_time()
  end

  def write_to_disk(fn = @full_path_file)
    @full_path_file = fn
    lbuf = buffer.text
    if VR::IDE::Settings[:editor][:insert_spaces_instead_of_tabs]
      lbuf.split("\t").join(" " * VR::IDE::Settings[:editor][:tab_width])
    else
      lbuf.split(" " * VR::IDE::Settings[:editor][:tab_width]).join("\t")
    end
    File.open(fn,"w") { |f| f.print(lbuf) }
    buffer.modified = false
    @modified_time = mod_time()
    @title.text = File.basename(fn)
    return true
  end

  def mod_time()
    return File.file?(@full_path_file) ? File.stat(@full_path_file).mtime : nil
  end
  
  def hilight_line(line_num)
    remove_tag("hilight")
    apply_tag_to_line(line_num, "hilight")
  end
  
  def reset_find()
    @last_find = nil
  end
  
  def find_backwards(text, options)
    s = @last_fild.nil? ? buffer.end_iter : @last_find
    e = buffer.start_iter
    flags = Gtk::TextIter::SOURCE_SEARCH_TEXT_ONLY
    flags |= Gtk::TextIter::SOURCE_SEARCH_CASE_INSENSITIVE unless options[:matchcase]
    found = false
    while (!found)
      ss, se = s.backward_search(text,flags,e)
      found = !(ss.nil?)
      if found && options[:matchword]
        found = ss.starts_word? && se.ends_word?
        if (!found)
          s = se
        end
      else
        break
      end
    end
    if (found && !ss.nil?)
      @last_find = se
      jump_to_line(ss.line,text)
    end
    
    return found
  end
  
  def find_forwards(text, options)
    s = @last_find.nil? ? buffer.start_iter : @last_find
    e = buffer.end_iter
    flags = Gtk::TextIter::SOURCE_SEARCH_TEXT_ONLY
    flags |= Gtk::TextIter::SOURCE_SEARCH_CASE_INSENSITIVE unless options[:matchcase]
    found = false
    while (!found)
      ss, se = s.forward_search(text,flags,e)
      found = !(ss.nil?)
      if found && options[:matchword]
        found = ss.starts_word? && se.ends_word?
        if (!found)
          s = se
        end
      else
        break
      end
    end
    if (found && !ss.nil?)
      @last_find = se
      jump_to_line(ss.line,text)
    end
    
    return found
  end
  
  private :find_backwards, :find_forwards
  
  def find_text(text,options={:matchcase => false,:matchword => false,:backwards => false,:regex => false,:wraparound => true})
    ret = options[:backwards] ? find_backwards(text,options) : find_forwards(text,options)
    if ret == false && options[:wraparound]
      reset_find()
      ret = options[:backwards] ? find_backwards(text,options) : find_forwards(text,options)
    end
    return ret
  end
  
  def replace_text(text)
    start, stop, selected = buffer.selection_bounds
    if selected
      buffer.delete(start,stop)
      buffer.insert_at_cursor(text)
      buffer.modified = true
      @last_find = nil # Cause we modified the buffer, so @last_find no longer valid
      true
    else
      false
    end
  end
  
  def jump_to_line(line_num, search_str = "")
    hilight_line(line_num)
    iter = buffer.get_iter_at_line(line_num)
    mark = buffer.create_mark("error",iter,true)
    scroll_to_mark(mark,0.0,true,0.0,0.5)
    buffer.place_cursor(iter)
    scroll_to_cursor
    select_text(line_num,search_str)
  end
end
