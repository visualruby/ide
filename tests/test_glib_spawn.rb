require 'glib2'

#cmd = GLib::Spawn.async("ruby test_shell.rb","",Dir.pwd,GLib::Spawn::DO_NOT_REAP_CHILD)
glib_env = []
ENV.each do |key,val|
  glib_env << "#{key}=#{val}"
end

lpid, lin, lout, lerr = GLib::Spawn.async_with_pipes(Dir.pwd,["ruby","test_shell.rb"],glib_env,GLib::Spawn::SEARCH_PATH|GLib::Spawn::DO_NOT_REAP_CHILD)

@thread = Thread.new do
  Thread.current[:exited] = false
  rs_signal = {lout => "stdout", lerr => "stderr"}
  loop do
    rs, _ = IO.select([lout,lerr],nil,nil,0.5)
    unless (rs.nil? or rs.empty?)
      rs.each do |input|
        data = input.gets
        puts "#{rs_signal[input]}: #{data}" unless data.nil?
      end
    end
    break if Thread.current[:exited]
  end
end

ml = GLib::MainLoop.new

GLib::ChildWatch.add(lpid) do
  @thread[:exited] = true
  @thread.join
  puts "Process #{lpid} exited."
  ml.quit
end

ml.run