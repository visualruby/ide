require 'webrick'
require 'erb'
require 'require_all'

$THE_PATH = File.expand_path(File.dirname(__FILE__))
require_all Dir.glob(File.join($THE_PATH,"rbdocs","**","*.rb"))

WEBrick::HTTPUtils::DefaultMimeTypes['rhtml'] = 'text/html'

# Simple WEBrick server, that serves just HTML files.
# You can add Servlets under rbdocs

server = WEBrick::HTTPServer.new(
  :Port => 4000,
  :DocumentRoot => File.join($THE_PATH,"htdocs")
)

server.mount("/",WEBrick::HTTPServlet::FileHandler,File.join($THE_PATH,"htdocs"))
server.mount("/rubyinfo",RubyInfoServlet)
server.mount("/servlets/<%= app_class_name %>",<%= app_class_name %>)

trap("INT") { server.shutdown }
server.start