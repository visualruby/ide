require 'module.rb'

class Test
  attr_reader :ivar1
  attr_accessor :ivar2
  def initialize
    puts "Hello World"
    @ivar1 = 32
    @ivar2 = 42
  end

  def goodbye
    puts "Goodbye Cruel World!"
  end

  alias_method :goodbye, :eol
end
