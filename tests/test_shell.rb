STDOUT.sync = true
STDERR.sync = true

Thread.new do
  (0..10).each do |x|
    sleep 0.5
    puts "Thread 1: #{x}"
  end
end

Thread.new do
  (100..110).each do |y|
    sleep 0.2
    puts "Thread 2: #{y}"
  end
end

Thread.new do
  (300..310).each do |e|
    sleep 0.3
    STDERR.puts "Thread 3: #{e}"
  end
end

(500..510).each do |z|
  sleep 0.1
  puts "Thread M: #{z}"
end
